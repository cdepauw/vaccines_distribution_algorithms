"""
Identity: Cédrick DE PAUW
Student_ID: 000397980
Affiliation: ULB
"""

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from scipy.stats import norm
import numpy as np

x = np.arange(200)+1
y = norm.pdf(x, loc=x.size/2, scale=x.size/4)

plt.xlabel("Time slot position")
plt.ylabel("Probability")
plt.plot(x,y)
plt.savefig('figures/normal_distribution.png', bbox_inches='tight', seed=123)
