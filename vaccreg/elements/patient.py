"""
Identity: Cédrick DE PAUW
Affiliation: ULB
"""

# ---------
# Libraries
# ---------
import numpy as np

# ---------
# Constants
# ---------
from vaccreg.constants import *


# -----
# Class
# -----

class Patient:
    """Patient Class.

    This class aims to contain all information required for a patient to obtain
    appointment from one of the implemented algorithms.
    """

    __slots__ = {'patient_id', 'center', 'slot', 'matched', 'happiness',
                 'next_proposal', 'proposed_all', 'cohort_type', 'priority',
                 'min_indices', 'max_indices', 'slots', 'selected_slots',
                 'sorted_slots'}

    def __init__(self, patient_id, cohort_type, priority, slots, si):
        """Initialize a `SharedInterface` instance.

        Args:
            patient_id: unique ID of the patient.
            cohort_type: cohort from which the patient comes.
            priority: priority associated to the patient.
            slots: numpy array of preferences for each slot.
            si: shared interface.
        """
        # Patient's ID.
        self.patient_id = patient_id
        # Matched center.
        self.center = -1
        # Matched slot.
        self.slot = -1
        # Verify the statement "it is matched".
        self.matched = False
        # Happiness obtained by patient's matching.
        self.happiness = 0
        # Next proposal after being rejected.
        self.next_proposal = 0
        # Check if proposed to all their selection.
        self.proposed_all = False
        # Cohort type.
        self.cohort_type = cohort_type
        # Priority.
        self.priority = priority
        # Minimum indices of slots for each center.
        self.min_indices = si.min_index_per_center
        # Maximum indices of slots for each center.
        self.max_indices = si.max_index_per_center
        # Selected slots and corresponding preferences.
        self.slots = slots
        # Number of selected slots.
        self.selected_slots = (self.slots > 0).sum()
        # Sorted slots by preferences.
        self.sorted_slots = self.slots.argsort()[::-1]
        # Determine patient's happiness.
        self.determine_happiness()
        # Determine the right next (first) proposal.
        self.check_next_proposal()
        #print("Patient %d done." % patient_id)

    def match(self, center, slot, preference):
        """Match the patient with the slot from a center.
        
        Args:
            center: center of the matched slot.
            slot: slot to which patient is matched.
            preference: patient's preference for the slot.
        """
        self.center = center
        self.slot = slot
        self.matched = True
        self.determine_happiness(preference)

    def unmatch(self):
        """Unmatch the patient from their slot."""
        self.center = -1
        self.slot = -1
        self.matched = False
        #self.happiness = 0
        self.determine_happiness()

    def determine_happiness(self, preference=None):
        """Determine patient's happiness based on their matching.
        
        Args:
            preference: preference for a matched slot, if there is one.
        """
        # If patient did not selected a slot.
        if not self.selected_slots:
            self.happiness = 0
        else:
            # If it is matched.
            if self.matched:
                self.happiness = preference
            # If it is not matched.
            else:
                self.happiness = 0

    def ravel(self, center_idx, slot_idx):
        """Compute flat 1D array index from 2D array indices.
        
        Args:
            center_idx: index of the center.
            slot_idx: index of the slot.

        Returns:
            1D array index for the indicated slot.
        """
        return self.max_indices[center_idx] + slot_idx

    def unravel(self, index):
        """Compute flat 2D array indices from 1D array index.
        
        Args:
            index: index of the slot in the general slots list.

        Returns:
            2D array indices for the indicated slot.
        """
        center_idx = np.argwhere(self.max_indices > index).flatten()[0]
        slot_idx = index - self.min_indices[center_idx]
        return center_idx, slot_idx

    def get_next_proposal(self):
        """Get patient's next proposal from its preference list.

        Returns:
            Tuple formed by the center, the slot and the preference.
        """
        # Get next (center, slot) pair and the corresponding preference.
        new_index = self.sorted_slots[self.next_proposal]
        center, slot = self.unravel(new_index)
        pref = self.slots[new_index]

        # Prepare next proposal index.
        self.next_proposal += 1
        # Check next proposal.
        self.check_next_proposal()

        # Return desired results.
        return center, slot, pref
    
    def check_next_proposal(self):
        """Check patient's next proposal from its preference list."""
        # Check if already proposed all selected slots.
        # Check next best proposal.
        new_index = self.sorted_slots[self.next_proposal]
        # Get preference for proposal.
        pref = self.slots[new_index]
        # If slot is rejected.
        if pref <= 0:
            # Do not propose anymore.
            self.proposed_all = True
