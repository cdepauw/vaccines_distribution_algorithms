"""
Identity: Cédrick DE PAUW
Affiliation: ULB
"""

# ---------
# Libraries
# ---------
import numpy as np

# ---------
# Constants
# ---------
from vaccreg.constants import *


# -------
# Classes
# -------

class VaccinationCenter:
    """Vaccination Center Class.
     
     This class aims to simulate a vaccination center, the filter it can
     perform on patient if necessary and to distribute vaccination center
     time slots.
    """

    __slots__ = {"nr_slots", "pace", "slots", "priorities", "filtering"}

    def __init__(self, slots, pace, filtering):
        """Initialize a `VaccinationCenter` instance.

        Args:
            slots: number of slots available.
            pace: number of vaccines available per slots.
            filtering: list of integers answering the question
                "will it filter a part of the patients or not?".
        """
        # Number of slots in the center.
        self.nr_slots = slots
        # Number of duplicates for each slot in the center.
        self.pace = pace
        # List of slots.
        self.slots = np.full(self.nr_slots, -1, dtype=np.int)
        # List of priorities, from matched patients to each slot.
        self.priorities = np.full(self.nr_slots, -1, dtype=np.int8)
        # Filtering to use.
        self.filtering = filtering

    def is_eligible(self, patient_cohort):
        """Determine if a patient is eligible based on its cohort type.

        A patient may not be eligible if the vaccination center filters some
        categories of patients.
        
        Args:
            patient_cohort: risk group of the patient.

        Returns:
            A boolean value indicating if the patient is eligible.
        """
        if patient_cohort in self.filtering:
            return False
        else:
            return True

    def has_priority(self, slot, patient_priority):
        """Determine if a patient has priority on a slot.

        Tells if a patient has priority over another patient for a given
        slot based on the priority of their risk group.
        
        Args:
            slot: slot for which priority should be determined.
            patient_priority: priority of the patient's cohort.

        Returns:
            A boolean value indicating if the patient has priority.
        """
        if self.priorities[slot] < patient_priority:
            return True
        else:
            return False

    def is_matched(self, slot):
        """Verify if a slot is already matched or not.

        Args:
            slot: slot to check.

        Returns:
            A boolean value indicating if the slot is matched or not.
        """
        if self.slots[slot] == -1:
            return False
        else:
            return True

    def matched_patient(self, slot):
        """Provide the patient matched to a slot.

        Args:
            slot: slot from which the matched patient is expected.

        Returns:
            The patient matching the slot.
        """
        return self.slots[slot]

    def match(self, slot, patient_id, patient_priority):
        """Match a patient to a slot.

        Args:
            slot: slot to match to the patient.
            patient_id: ID of the patient to match to the slot.
            patient_priority: priority of the patient to match.
        """
        # Assign patient ID to the corresponding slot.
        self.slots[slot] = patient_id
        # Keep track of matched patient's priority for each slot.
        self.priorities[slot] = patient_priority
