"""
Identity: Cédrick DE PAUW
Affiliation: ULB
"""

# ---------
# Libraries
# ---------
import xlrd
import numpy as np


# ---------
# Constants
# ---------

# Default seed.
SEED = 12345
# Minimum priority value.
MIN_PRIORITY = 1
# Maximum priority value.
MAX_PRIORITY = 2
# High priority part of the population in Belgium.
PRIORITY_POPULATION = 4006739  # see SHC source
# Low priority part of the population in Belgium.
ELIGIBLE_POPULATION = 11492641  # see Statbel
# Priority proportions.
PRIOR_DISTRIB = [(ELIGIBLE_POPULATION-PRIORITY_POPULATION)/ELIGIBLE_POPULATION,
                 PRIORITY_POPULATION/ELIGIBLE_POPULATION]
# Range of priority values.
PRIORITY_SPACE = np.arange(MIN_PRIORITY, MAX_PRIORITY+1).astype(np.int8)

# Minimum cohort type.
MIN_COHORT_TYPE = 1
# Maximum cohort type.
MAX_COHORT_TYPE = 5
