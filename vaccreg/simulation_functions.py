"""
Identity: Cédrick DE PAUW
Affiliation: ULB
"""


# ---------
# Libraries
# ---------
from vaccreg.constants import *
from vaccreg import *
import time

# ---------
# Functions
# ---------

def sm_simulation(nr_centers, centers_slots, nr_patients,
        seed, distrib, nr_selected = -1):
    """Perform a simulation using the SMTI analogy, using the GS algorithm.

    Args:
        nr_centers: number of centers.
        centers_slots: number of slots per center.
        nr_patients: number of patients.
        seed: seed to use for the rng.
        distrib: string indicated the distribution to use for the selection.
        nr_selected: number of slots patients should select.

    Returns:
        Tuple containing, in order:
            - list of matching ratio for each priority,
            - mean happiness,
            - std happiness.
    """
    # Create shared interface and initialize its seed.
    si = shared_interface.SharedInterface(nr_centers,
                                          centers_slots,
                                          seed, distrib)
    si.set_seed(seed)

    # Initialize a graph generator.
    gg = graph_generator.GraphGenerator(si)


    #print("Patients initialization...")
    # Initialize patients cohorts.
    cohorts = si.rng.integers(low=MIN_COHORT_TYPE,
                              high=MAX_COHORT_TYPE,
                              size=nr_patients,
                              dtype=np.int8,
                              endpoint=True)
    # Initialize patients priorities.
    priorities = si.rng.choice(PRIORITY_SPACE,
                               size=nr_patients,
                               replace=True,
                               p=PRIOR_DISTRIB).astype(np.int8)

    # Generate slots.
    slots = gg.repeat_generation(nr_patients, False, False, nr_selected)

    # Generate patients list.
    patients = [patient.Patient(i, cohorts[i], priorities[i], slots[i,:], si)
                for i in range(nr_patients)]

    #print("Centers initialization...")
    # Generate vaccination centers list.
    centers = [vaccination_center.VaccinationCenter(si.nr_slots[i], 1, [])
               for i in range(nr_centers)]

    #print("Solving...")
    # Measure time before solving
    start = time.process_time()

    # Prepare algo. and run it
    algo = algorithms.GaleShapley(si, patients, centers)
    algo.run()

    # Measure time elapsed (in seconds)
    end = time.process_time()
    delta = end - start

    #print("Solved in %f seconds." % delta)

    all_happiness = np.zeros(nr_patients)
    matched = 0
    matched_patients = [0 for _ in PRIORITY_SPACE]
    total_patients = [0 for _ in PRIORITY_SPACE]
    patients_ratio = [0 for _ in PRIORITY_SPACE]
    for i in range(nr_patients):
        patient_priority = patients[i].priority - 1
        if patients[i].matched:
            matched += 1
            matched_patients[patient_priority] += 1

        total_patients[patient_priority] += 1

        all_happiness[i] = patients[i].happiness

    for i in range(len(total_patients)):
        if matched_patients[i] != 0:
            patients_ratio[i] = matched_patients[i]/total_patients[i]
        else:
            patients_ratio[i] = 0

    total_happiness = all_happiness.sum()
    average_happiness = all_happiness.mean()
    std_happiness = all_happiness.std()
    success = matched / nr_patients
    #print("Total happiness: %f" % total_happiness)
    #print("Mean happiness: %f" % average_happiness)
    #print("Std happiness: %f" % std_happiness)
    #print("Success: %f" % success)

    return patients_ratio, average_happiness, std_happiness

def mcm_simulation(nr_centers, centers_slots, nr_patients,
        seed, distrib, nr_selected = -1):
    """Perform a simulation using the MCM analogy.

    Args:
        nr_centers: number of centers.
        centers_slots: number of slots per center.
        nr_patients: number of patients.
        seed: seed to use for the rng.
        distrib: string indicated the distribution to use for the selection.
        nr_selected: number of slots patients should select.

    Returns:
        Tuple containing, in order:
            - list of matching ratio for each priority,
            - mean happiness,
            - std happiness.
    """
    # Create shared interface and initialize its seed.
    si = shared_interface.SharedInterface(nr_centers,
                                          centers_slots,
                                          seed, distrib)
    si.set_seed(seed)

    # Initialize a graph generator.
    gg = graph_generator.GraphGenerator(si)


    #print("Graph initialization...")
    # Initialize patients cohorts.
    cohorts = si.rng.integers(low=MIN_COHORT_TYPE,
                              high=MAX_COHORT_TYPE,
                              size=nr_patients,
                              dtype=np.int8,
                              endpoint=True)
    # Initialize patients priorities.
    priorities = si.rng.choice(PRIORITY_SPACE,
                               size=nr_patients,
                               replace=True,
                               p=PRIOR_DISTRIB).astype(np.int8)

    # Generate slots.
    slots = gg.repeat_generation(nr_patients, False, False, nr_selected)

    #print("Solving...")
    # Measure time before solving
    start = time.process_time()

    # Prepare algo. and run it
    algo = algorithms.HopcroftKarp(slots)
    p_matching, s_matching = algo.run()

    # Measure time elapsed (in seconds)
    end = time.process_time()
    delta = end - start

    #print("Solved in %f seconds." % delta)

    only_matched = p_matching != -1
    
    matched = (p_matching != -1).sum()
    matched_patients = [0 for _ in PRIORITY_SPACE]
    total_patients = [0 for _ in PRIORITY_SPACE]
    patients_ratio = [0 for _ in PRIORITY_SPACE]
    for i, priority in enumerate(PRIORITY_SPACE):
        matched_patients[i] = (priorities[only_matched] == priority).sum()
        total_patients[i] = (priorities == priority).sum()

        if matched_patients[i] != 0:
            patients_ratio[i] = matched_patients[i]/total_patients[i]
        else:
            patients_ratio[i] = 0

    happiness_indices = (np.where(only_matched), p_matching[only_matched])
    all_happiness = np.zeros(nr_patients, dtype=np.int)
    all_happiness[happiness_indices[0]] = slots[happiness_indices]
    total_happiness = all_happiness.sum()
    average_happiness = all_happiness.mean()
    std_happiness = all_happiness.std()
    success = matched / nr_patients
    #print("Total happiness: %f" % total_happiness)
    #print("Mean happiness: %f" % average_happiness)
    #print("Std happiness: %f" % std_happiness)
    #print("Success: %f" % success)

    return patients_ratio, average_happiness, std_happiness

def sep_mcm_simulation(nr_centers, centers_slots, nr_patients,
        seed, distrib, nr_selected = -1):
    """Perform a simulation using the MCM analogy based on subgraphs.

    Args:
        nr_centers: number of centers.
        centers_slots: number of slots per center.
        nr_patients: number of patients.
        seed: seed to use for the rng.
        distrib: string indicated the distribution to use for the selection.
        nr_selected: number of slots patients should select.

    Returns:
        Tuple containing, in order:
            - list of matching ratio for each priority,
            - mean happiness,
            - std happiness.
    """
    # Create shared interface and initialize its seed.
    si = shared_interface.SharedInterface(nr_centers,
                                          centers_slots,
                                          seed, distrib)
    si.set_seed(seed)

    # Initialize a graph generator.
    gg = graph_generator.GraphGenerator(si)


    #print("Graph initialization...")
    # Initialize patients cohorts.
    cohorts = si.rng.integers(low=MIN_COHORT_TYPE,
                              high=MAX_COHORT_TYPE,
                              size=nr_patients,
                              dtype=np.int8,
                              endpoint=True)
    # Initialize patients priorities.
    priorities = si.rng.choice(PRIORITY_SPACE,
                               size=nr_patients,
                               replace=True,
                               p=PRIOR_DISTRIB).astype(np.int8)

    # Generate slots.
    slots = gg.repeat_generation(nr_patients, False, False, nr_selected)

    #print("Solving...")
    p_matchings = [[] for _ in PRIORITY_SPACE]
    s_matchings = [[] for _ in PRIORITY_SPACE]

    # Measure time before solving
    start = time.process_time()

    # Prepare algo. and run it for each priority, in descending order.
    for i, priority in reversed(list(enumerate(PRIORITY_SPACE))):
        slots_copy = slots.copy()
        filter_priority = priorities != priority
        slots_copy[filter_priority,:] = 0
        for s_matching in s_matchings:
            if s_matching != []:
                matched_slots = s_matching != -1
                slots_copy[:,matched_slots] = 0
        algo = algorithms.HopcroftKarp(slots_copy)
        p_matching, s_matching = algo.run()
        p_matchings[i] = p_matching
        s_matchings[i] = s_matching

    # Measure time elapsed (in seconds)
    end = time.process_time()
    delta = end - start

    #print("Solved in %f seconds." % delta)

    happiness_indices = (None, None)
    matched_patients = [0 for _ in PRIORITY_SPACE]
    total_patients = [0 for _ in PRIORITY_SPACE]
    patients_ratio = [0 for _ in PRIORITY_SPACE]
    for i, priority in enumerate(PRIORITY_SPACE):
        p_matching = p_matchings[i]
        only_matched = p_matching != -1
        matched = (p_matching != -1).sum()
        matched_patients[i] = (priorities[only_matched] == priority).sum()
        total_patients[i] = (priorities == priority).sum()

        if matched_patients[i] != 0:
            patients_ratio[i] = matched_patients[i]/total_patients[i]
        else:
            patients_ratio[i] = 0
        
        if i == 0:
            happiness_indices = (np.where(only_matched),
                                 p_matching[only_matched])
        else:
            new_hap0 = np.append(happiness_indices[0],
                                 np.where(only_matched))
            new_hap1 = np.append(happiness_indices[1],
                                 p_matching[only_matched])
            happiness_indices = (new_hap0, new_hap1)
    
    matched = sum(matched_patients)

    # NOTE: be aware that, in the opposite of the two previous methods,
    # here all_happiness is not ordered by patient_id
    all_happiness = np.zeros(nr_patients, dtype=np.int)
    all_happiness[happiness_indices[0]] = slots[happiness_indices]
    total_happiness = all_happiness.sum()
    average_happiness = all_happiness.mean()
    std_happiness = all_happiness.std()
    success = matched / nr_patients
    #print("Total happiness: %f" % total_happiness)
    #print("Mean happiness: %f" % average_happiness)
    #print("Std happiness: %f" % std_happiness)
    #print("Success: %f" % success)

    return patients_ratio, average_happiness, std_happiness
