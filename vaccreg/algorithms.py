"""
Identity: Cédrick DE PAUW
Affiliation: ULB
"""

# Avoid importing unnecessary or not ready classes
__all__ = ['GaleShapley', 'HopcroftKarp']

# ---------
# Libraries
# ---------
from scipy.sparse import csr_matrix
from scipy.sparse.csgraph import maximum_bipartite_matching


# -------
# Classes
# -------

class Algorithm:
    """Algorithm Class.

    This class is a parent class to all algorithms that will require access
    to the shared interface and to `Patient` and `VaccinationCenter` objects.
    """

    def __init__(self, shared_interface, patients, centers):
        """Initialize an `Algorithm` instance.

        Args:
            shared_interface: shared interface to communicate between elements.
            patients: list of patients.
            centers: list of vaccination centers.
        """
        # Shared interface.
        self.si = shared_interface
        # List of patients.
        self.patients = patients
        # List of vaccination centers.
        self.centers = centers

class GaleShapley(Algorithm):
    """Gale-Shapley Class.

    This class aims to solve a SM problem using the Gale-Shapley algorithm.
    """

    def __init__(self, *args):
        """Initialize a `GaleShapley` instance."""
        super().__init__(*args)

    def perform_iteration(self):
        """Perform an iteration of the GS algorithm."""
        for patient in self.patients:
            if not patient.matched and not patient.proposed_all:
                # Get new (center, slot) pair and corresponding preference.
                c, slot, pref = patient.get_next_proposal()
                # Get patient priority.
                priority = patient.priority
                # Get patient cohort type.
                cohort_type = patient.cohort_type

                # Check if the desired slot for the given center is already
                # matched.
                if self.centers[c].is_matched(slot):
                    # Check if patient has priority over the slot.
                    has_priority = self.centers[c].has_priority(slot, priority)
                    # Check if patient is eligible.
                    is_eligible = self.centers[c].is_eligible(cohort_type)

                    # Check if both conditions are satisfied.
                    if has_priority and is_eligible:
                        # Backup the old patient ID.
                        old_patient = self.centers[c].matched_patient(slot)
                        # Unmatch the old patient.
                        self.patients[old_patient].unmatch()
                        # Match the center's slot with the new patient.
                        self.centers[c].match(slot,
                                              patient.patient_id,
                                              patient.priority)
                        # Match the new patient with the center and the slot.
                        patient.match(c, slot, pref)
                        
                # Desidered slot for the given center is not matched.
                else:
                    # Match the center's slot with the patient.
                    self.centers[c].match(slot,
                                          patient.patient_id,
                                          patient.priority)
                    # Match the patient with the center and the slot.
                    patient.match(c, slot, pref)
                
    def check_unmatched(self):
        """Check for unmatched patients.

        Returns:
            Number of unmatched patients.
        """
        unmatched = 0
        for patient in self.patients:
            if not patient.matched and not patient.proposed_all:
                unmatched += 1
        return unmatched

    def run(self):
        """Run the algorithm."""
        iteration = 0
        running = True
        while running:
            self.perform_iteration()
            # If there are unmatched patients, continue.
            running = self.check_unmatched()

class HopcroftKarp:
    """Hopcroft-Karp Class.

    This class aims to allow the use of the Hopcroft-Karp algorithm, being
    available in the SciPy package, to solve the MCM.
    """

    def __init__(self, matrix):
        """Initialize a `HopcroftKarp` instance.

        Args:
            matrix: 2D array wherein first indices are patients and second
                indices are slots.
        """
        # SciPy graph of the problem: sparse matrix.
        self.graph = csr_matrix(matrix)

    def run(self):
        """Run the algorithm.

        Returns:
            Matched patients, matched slots.
        """
        # Patients matchings.
        matched_col = maximum_bipartite_matching(self.graph,
                                                 perm_type='column')
        # Time slots matchings.
        matched_row = maximum_bipartite_matching(self.graph,
                                                 perm_type='row')
        return matched_col, matched_row
