"""
Identity: Cédrick DE PAUW
Affiliation: ULB
"""

# ---------
# Libraries
# ---------
import numpy as np
import os
import random


# -----
# Class
# -----

class GraphGenerator:
    """Graph Generator Class.

    This class aims to generate connections between the two sets of vertices
    of the problem and to associate them weights.
    """

    def __init__(self, shared_interface):
        """Initialize a `GraphGenerator` instance.

        Args:
            shared_interface: shared interface.
        """
        self.si = shared_interface

    def set_connections(self, nr_selected=-1):
        """Set connections between the patient and slots.

        Args:
            nr_selected: number of slots to select by the patient.

        Returns:
            Patient's binary preference list.
        """
        return self.with_incomplete_lists(nr_selected)

    def with_complete_lists(self, nr_selected=-1):
        """Create a complete preference list for the patient.

        Args:
            nr_selected: number of slots to select by the patient.

        Returns:
            Patient's binary preference complete list.
        """
        # Select all centers.
        sel_centers = np.ones(self.si.nr_centers, dtype=np.bool)
        # Initialize slots to unselected.
        sel_slots = np.ones(self.si.nr_prefs, dtype=np.int)
        return sel_centers, sel_slots

    def with_incomplete_lists(self, nr_selected=-1):
        """Create an incomplete preference list for the patient.

        Args:
            nr_selected: number of slots to select by the patient.

        Returns:
            Patient's binary preference incomplete list.

        Raises:
            Exception: in case the number of selected slots if larger than
                what is available.
        """
        # If the number of slots to select is higher than the available
        # number of slots.
        if nr_selected > self.si.nr_prefs:
            raise Exception("Fixed number of selected slots is too high: " +\
                            "%d instead of maximum " % nr_selected +\
                            "%d slots." % self.si.nr_prefs)
        # Initialize the number of slots to select if not fixed
        if nr_selected == -1:
            nr_selected = self.si.rng.integers(
                    1,
                    self.si.nr_prefs,
                    dtype=np.int)

        # Alias for distributions to use.
        c_distrib = self.si.centers_rejection_distrib
        s_distrib = self.si.slots_distrib

        # Select centers based on a given distribution.
        sel_centers = self.si.rng.choice(np.array([False, True],
                                         dtype=np.bool),
                                         size=self.si.nr_centers,
                                         replace=True,
                                         p=c_distrib)
        while self.si.nr_slots[sel_centers].sum() < nr_selected:
            sel_centers = self.si.rng.choice(np.array([False, True],
                                             dtype=np.bool),
                                             size=self.si.nr_centers,
                                             replace=True,
                                             p=c_distrib)
        
        # Initialize list of slots from which to select.
        filtered_indices = np.zeros(self.si.nr_prefs, dtype=np.bool)
        # Filter indices depending on selected centers.
        for c, selected in enumerate(sel_centers):
            if selected:
                filter_low_indices =\
                        self.si.list_indices >= self.si.min_index_per_center[c]
                filter_high_indices =\
                        self.si.list_indices < self.si.max_index_per_center[c]
                c_indices = filter_low_indices & filter_high_indices
                filtered_indices |= c_indices
        candidate_slots = np.argwhere(filtered_indices)

        # Initialize slots to unselected.
        sel_slots = np.zeros(self.si.nr_prefs, dtype=np.int)
        selection = np.array([])
        # Compute the adapted distribution for slots of selected centers.
        sel_distrib = s_distrib[sel_centers].flatten()
        sel_distrib = sel_distrib/sel_distrib.sum()
        # Select randomly among all existing slots from all selected centers
        # based on a given distribution.
        selection = self.si.rng.choice(
                candidate_slots,
                size=nr_selected,
                p=sel_distrib,
                replace=False
                )
        sel_slots[selection] = 1
        return sel_centers, sel_slots

    def set_weights(self, slots, nr_selected=-1):
        """Set weights on connections between the patient and slots.

        Args:
            slots: boolean list indicating slots which are connected.
            nr_selected: number of slots to select by the patient.

        Returns:
            Patient's preference list.
        """
        return self.without_ties(slots, nr_selected=-1)

    def without_ties(self, slots, nr_selected=-1):
        """Set weights without ties in patient's preference list.

        Args:
            slots: boolean list indicating slots which are connected.
            nr_selected: number of slots to select by the patient.

        Returns:
            Patient's preference list without ties.
        """
        # Get the number of selected slots.
        nr_sel_slots = int(slots.sum())

        # Generate preferences for selected slots.
        prefs = np.array([])
        if nr_selected != -1:
            prefs = np.arange(nr_sel_slots) + 1
        else:
            prefs = np.arange(self.si.nr_prefs - nr_sel_slots,
                              self.si.nr_prefs
                    ) + 1
        # Shuffle the preferences
        self.si.rng.shuffle(prefs)

        # Update the slots preferences.
        slots[slots > 0] = np.multiply(slots[slots > 0], prefs)
        return slots

    
    def with_ties(self, slots, nr_selected=-1):
        """Set weights with ties in patient's preference list.

        Args:
            slots: boolean list indicating slots which are connected.
            nr_selected: number of slots to select by the patient.

        Returns:
            Patient's preference list with ties.
        """
        # Get the number of selected slots.
        nr_sel_slots = int(slots.sum())
        
        # Generate preferences.
        prefs = np.array([])
        if nr_selected != -1:
            prefs = np.arange(nr_sel_slots) + 1
        else:
            prefs = np.arange(self.si.nr_prefs - nr_sel_slots,
                              self.si.nr_prefs
                    ) + 1

        space_cs = prefs.cumsum()
        space_cs_sum = space_cs.sum()
        space_cs_distrib = space_cs/space_cs_sum
        # Distribution
        distrib = space_cs_distrib

        preferences = self.si.rng.choice(prefs,
                                         size=nr_sel_slots,
                                         replace=True,
                                         p=distrib)

        # Get occurences for each value.
        bins = np.bincount(preferences, minlength = self.si.nr_prefs+1)
        bins = bins[1:]  # remove the "0" preference value

        # Get preference values for which there were no occurences.
        where_zero = np.where(bins == 0)[0] + 1
        # Check where preferences are different from zero.
        non_zero_prefs = preferences > 0

        # Increase values such that a set of continuous values is used.
        for i in where_zero:
            preferences[(preferences <= i) & non_zero_prefs] += 1

        # Update the slots preferences.
        slots[slots > 0] = np.multiply(slots[slots > 0], preferences)
        return slots

    def repeat_generation(self, repeat, complete, ties, nr_selected = -1):
        """Repeat preference list generation for each patient.

        Args:
            repeat: number of time a preference list should be created.
            complete: answer to question "should all slots be selected?"
            ties: answer to question "are ties allowed or not?"
            nr_selected: number of slots to select for each patient.

        Returns:
            All patients preference lists.
        """
        connections_func = None
        if complete == True:
           connections_func = self.with_complete_lists 
        else:
           connections_func = self.with_incomplete_lists
        weights_func = None
        if ties == True:
            weights_func = self.with_ties
        else:
            weights_func = self.without_ties

        slots = np.zeros((repeat, self.si.nr_prefs), dtype=np.int)
        for i in range(repeat):
            _, s = connections_func(nr_selected)
            slots[i] = weights_func(s, nr_selected)
        return slots
