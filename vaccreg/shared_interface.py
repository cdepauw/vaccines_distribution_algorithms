"""
Identity: Cédrick DE PAUW
Affiliation: ULB
"""

# ---------
# Libraries
# ---------
import numpy as np
import os
import random

# ---------
# Constants
# ---------
from vaccreg.constants import *
from scipy.stats import norm


# -----
# Class
# -----

class SharedInterface:
    """Shared Interface Class.

    This class aims to allow distinct parts of the software to share their
    parameters or their variables.
    """

    def __init__(self, nr_centers, nr_slots_per_center, seed,
                 distrib="uniform"):
        """Initialize a `SharedInterface` instance.

        Args:
            nr_centers: number of centers.
            nr_slots_per_center: list of the number of slots for each of the
                vaccination center.
            seed: seed to use for the random number generator.
            distrib: distribution used to build each random bipartite graph,
                accepted values are "uniform" or "normal". 
        """
        # Number of centers.
        self.nr_centers = nr_centers
        # Number of slots per center.
        self.nr_slots = np.array(nr_slots_per_center, dtype=np.int)
        #self.max_slots = max(nr_slots_per_center)
        # Total number of preferences to determine, i.e. number of slots.
        self.nr_prefs = sum(self.nr_slots)
        # Maximum (excluded) index for the slots of each vaccination center,
        # when slots from all centers are grouped in a 1D array.
        self.max_index_per_center = self.nr_slots.cumsum(dtype=np.int)
        # Minimum (included) index for the slots of each vaccination center,
        # when slots from all centers are grouped in a 1D array.
        self.min_index_per_center = np.roll(self.max_index_per_center, 1)
        self.min_index_per_center[0] = 0
        # List possible indices for the 1D array containing all slots.
        self.list_indices = np.arange(self.nr_prefs, dtype=np.int)
        # Set the seed for the rng.
        self.set_seed(seed)
        # Distribution to use to determine if a center is selected by a
        # patient or not.
        self.centers_rejection_distrib = [1/2, 1/2]
        # Distribution to use to select slots from a vaccination center.
        self.slots_distrib = np.array([])
        if distrib == "normal":
            # Create shape of a normal distribution on `nr_slots[i]` discrete
            # values (on the slots list), for each vaccination center.
            # Distribution center is located at the middle of the slots list
            # and scale is `nr_slots[i]/4`, for each vaccination center.
            normal = np.array([norm.pdf(np.arange(self.nr_slots[i]),
                                        loc=self.nr_slots[i]/2,
                                        scale=self.nr_slots[i]/4)
                               for i in range(self.nr_centers)])
            # Normalize each of these shapes to obtain normal distributions
            # for each vaccination center.
            normal = np.array([normal[i,:]/normal[i,:].sum()
                               for i in range(self.nr_centers)])
            self.slots_distrib = normal
        else:
            # Create a uniform distribution for eahc vaccination center.
            uniform = np.array([np.ones(self.nr_slots[i])/self.nr_slots[i]
                                for i in range(self.nr_centers)])
            self.slots_distrib = uniform

    def set_seed(self, seed):
        """Set the value of the seed.
        
        Args:
            seed: seed to use with the random number generator.
        """
        # NOTE: recall before each new simulation.
        self.seed = seed
        os.environ['PYTHONHASHSEED']=str(seed)
        random.seed(seed)
        np.random.seed(seed)
        self.rng = np.random.default_rng(seed)
