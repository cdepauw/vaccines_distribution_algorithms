# Vaccines distribution algorithms

## Description

This project was created for my thesis for the Master of science in Computer Science and Engineering with focus Professional at the École Polytechnique de Bruxelles (ULB, Brussels, Belgium).

It aims to help for the registration of citizens to vaccination time slots in order to get an appointment. Algorithms, i.e. Gale-Shapley algorithm and Hopcroft-Karp algorithm, run offline in order to get a matching between citizens and time slots.

These two algorithms are used to study how they perform with sparse bipartite graphs regarding matching sizes and citizens priorities and preferences.

### Erratum

Three figures, i.e. those on which mean happiness appears for the SMTI, were impacted by an error from a commented line leaving the happiness to the last obtained value when a patient became unmatched instead of resetting it to 0 as expected.

Nevertheless, the change after the correction is negligible and does not affect the observations nor the conclusions. The corrected version of the thesis is available as [2022\_Info\_DePauw.pdf](https://gitlab.com/cdepauw/vaccines_distribution_algorithms/-/blob/main/2022_Info_DePauw.pdf).

## Installation

This project was run with Python 3.7.4.

You may find a [requirements.txt](https://gitlab.com/cdepauw/vaccines_distribution_algorithms/-/blob/main/requirements.txt) file in order to install adequate Python libraries. Use the package manager `pip` to install dependencies.

Required packages for R scripts are *ggplot2* and *RColorBrewer*.


## Usage

Simulations may be performed using the following command:

```bash
python3 vaccines_distribution.py
```

For an interactive usage of the package, please import `vaccreg`. Example of usage are available in the `vaccreg` package, in the `simulation_functions.py` Python script.

## Author

**Author**: Cédrick De Pauw

**University**: ULB

## License

[MIT](https://choosealicense.com/licenses/mit/)
