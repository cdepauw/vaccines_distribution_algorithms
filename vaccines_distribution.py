"""
Identity: Cédrick DE PAUW
Affiliation: ULB
"""

# ---------
# Libraries
# ---------
import vaccreg
from vaccreg.simulation_functions import *
import os
import pandas as pd

# ---------
# Main Code
# ---------

if __name__ == '__main__':
    # Output file.
    output_file = "output/results.csv"
    # Create output directory.
    try:
        os.makedirs("output")
    except:
        # The directory already exists.
        pass

    # Parameters to use for the simulation.
    # Number of centers.
    nr_centers = 5
    # Number of slots per centers.
    centers_slots = [200, 200, 200, 200, 200]
    # Number of patients.
    nr_patients = 1000

    # Sizes to study for users selection.
    selections = [1, 5, 10, 20]
    # Time slot distribution used for users selection.
    distributions = ["uniform", "normal"]
    # Starting seed.
    seed = 123
    # Number of simulation to perform (i.e. distinct seeds to use).
    nr_iter = 20

    # Dictionary to store results.
    res_dict = {"Problem": [], "Distribution": [], "Selection Size": [],
                "Seed": [], "Matching Ratio 1": [], "Matching Ratio 2": [],
                "Happiness Mean": [], "Happiness Std": []}

    print("Run simulations...")
    print(f"Number of seeds to use: {nr_iter}.")

    # Try all defined distributions.
    for distrib in distributions:
        print(f"Distribution: {distrib}.")
        # Try all allowed number of selections.
        for sel in selections:
            print(f"- Number of selected time slots per user: {sel}.")
            
            # Solve the SMTI using Gale-Shapley algorithm.
            print("-- Solve the SMTI problem...")
            for s in range(seed, seed+nr_iter):
                #print(f"--- Current seed: {s}.")
                ratio, happ_mean, happ_std = sm_simulation(
                        nr_centers, centers_slots, nr_patients,
                        s, distrib, sel)
                instance = f"smti"
                res_dict["Problem"].append(instance)
                res_dict["Distribution"].append(distrib)
                res_dict["Selection Size"].append(sel)
                res_dict["Seed"].append(s)
                res_dict["Matching Ratio 1"].append(ratio[0])
                res_dict["Matching Ratio 2"].append(ratio[1])
                res_dict["Happiness Mean"].append(happ_mean)
                res_dict["Happiness Std"].append(happ_std)

            # Solve the MCM using Hopcroft-Karp algorithm.
            print("-- Solve the MCM problem...")
            for s in range(seed, seed+nr_iter):
                #print(f"--- Current seed: {s}.")
                ratio, happ_mean, happ_std = mcm_simulation(
                        nr_centers, centers_slots, nr_patients,
                        s, distrib, sel)
                instance = f"mcm"
                res_dict["Problem"].append(instance)
                res_dict["Distribution"].append(distrib)
                res_dict["Selection Size"].append(sel)
                res_dict["Seed"].append(s)
                res_dict["Matching Ratio 1"].append(ratio[0])
                res_dict["Matching Ratio 2"].append(ratio[1])
                res_dict["Happiness Mean"].append(happ_mean)
                res_dict["Happiness Std"].append(happ_std)

            # Solve the MCM using Hopcroft-Karp algorithm on subgraphs.
            print("-- Solve the MCM problem in separate steps...")
            for s in range(seed, seed+nr_iter):
                #print(f"--- Current seed: {s}.")
                ratio, happ_mean, happ_std = sep_mcm_simulation(
                        nr_centers, centers_slots, nr_patients,
                        s, distrib, sel)
                instance = f"sep_mcm"
                res_dict["Problem"].append(instance)
                res_dict["Distribution"].append(distrib)
                res_dict["Selection Size"].append(sel)
                res_dict["Matching Ratio 1"].append(ratio[0])
                res_dict["Matching Ratio 2"].append(ratio[1])
                res_dict["Seed"].append(s)
                res_dict["Happiness Mean"].append(happ_mean)
                res_dict["Happiness Std"].append(happ_std)

    # Store results.
    dataframe = pd.DataFrame.from_dict(res_dict)
    dataframe.to_csv(output_file, index=False)
    print(f"Results have been saved in {output_file}.")
